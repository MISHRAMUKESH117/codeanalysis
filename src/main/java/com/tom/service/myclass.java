class MyClass {
  private int x;
  private int y;

  public void setX(int val) { // Noncompliant: field 'x' is not updated
    this.y = val;
  }

  public int getY() { // Noncompliant: field 'y' is not used in the return value
    return this.x;
  }
   public int getX() { // Noncompliant: field 'y' is not used in the return value
    return this.x;
  }
   public int getZ() { // Noncompliant: field 'y' is not used in the return value
    return this.x;
  }
   
  
}